const express = require("express");
const router = express.Router();
const { requiresAuth } = require("express-openid-connect");
const { getAllSports } = require("../util/sports");

router.get("/", async (req, res) => {
    const sports = await getAllSports();

    res.render("index", {
        title: "Home",
        profile: req.oidc.isAuthenticated() && req.oidc.user,
        sports,
    });
});

router.get("/about", (req, res) => {
    res.render("about", {
        title: "About",
        profile: req.oidc.isAuthenticated() && req.oidc.user,
    });
});

router.get("/profile", requiresAuth(), (req, res) => {
    res.render("profile", {
        title: profile.name,
        profile: req.oidc.user,
    });
});

module.exports = router;

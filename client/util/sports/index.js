const axios = require("axios");

const getAllSports = async () => {
    try {
        const { data } = await axios.get("http://localhost:5000/api/sports");
        return data;
    } catch (error) {
        return error;
    }
};

module.exports = { getAllSports };

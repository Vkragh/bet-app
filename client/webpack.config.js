const path = require("path");

module.exports = {
    mode: "development",
    entry: ["./assets/js/index.js", "./assets/css/index.scss"],
    output: {
        filename: "js/index.js",
        path: path.resolve(__dirname, "dist"),
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [],
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "css/index.css",
                        },
                    },
                    "sass-loader",
                ],
            },
        ],
    },
};

const express = require("express");
require("dotenv").config();
const { auth } = require("express-openid-connect");

const PORT = process.env.PORT || 3000;

const app = express();

app.use(
    auth({
        authRequired: false,
        auth0Logout: true,
        issuerBaseURL: process.env.ISSUER_BASE_URL,
        baseURL: process.env.BASE_URL,
        clientID: process.env.CLIENT_ID,
        secret: process.env.SECRET,
    })
);

app.set("view engine", "ejs");

app.use("/", require("./routes"));

app.use(express.static(__dirname + "/dist"));

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));

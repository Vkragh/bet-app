const { MongoClient } = require("mongodb");
const { mongoUsername, mongoPassword } = require("../envVariables");

async function getCollection(documentCollection) {
    const client = await MongoClient.connect(
        `mongodb+srv://${mongoUsername}:${mongoPassword}@api.89mmt.mongodb.net/api?retryWrites=true&w=majority`
    );
    const db = client.db();

    const collection = db.collection(documentCollection);

    return {
        client,
        collection,
    };
}

module.exports = getCollection;

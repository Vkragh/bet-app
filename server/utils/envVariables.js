const apiUrl = process.env.API_BASE_URL;
const apiKeyName = process.env.API_KEY_NAME;
const apiKeyValue = process.env.API_KEY_VALUE;
const mongoUsername = process.env.MONGODB_USERNAME;
const mongoPassword = process.env.MONGODB_PASSWORD;

module.exports = {
    apiUrl,
    apiKeyName,
    apiKeyValue,
    mongoUsername,
    mongoPassword,
};

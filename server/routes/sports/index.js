const express = require("express");
const router = express.Router();
const axios = require("axios");
const apiCache = require("apicache");
const getCollection = require("../../utils/mongodb/getCollection");
const { apiUrl, apiKeyName, apiKeyValue } = require("../../utils/envVariables");

let cache = apiCache.middleware;

router.get("/", cache("60 minutes"), async (req, res) => {
    try {
        const { client, collection } = await getCollection("sports");
        const sports = await collection.find().toArray();
        if (sports) {
            client.close();
            return res.status(200).json(sports);
        }

        const params = new URLSearchParams({
            [apiKeyName]: apiKeyValue,
        });

        const apiRes = await axios.get(`${apiUrl}?${params}`);
        const apiResData = apiRes.data;
        const result = await collection.insertMany(apiRes.data);
        console.log(result);
        client.close();

        return res.status(200).json(apiResData);
    } catch (error) {
        res.status(500).json({ error });
    }
});

module.exports = router;

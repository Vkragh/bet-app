const url = require("url");
const express = require("express");
const router = express.Router();
const axios = require("axios");
const apiCache = require("apicache");
const getCollection = require("../../utils/mongodb/getCollection");
const { apiUrl, apiKeyName, apiKeyValue } = require("../../utils/envVariables");

let cache = apiCache.middleware;

router.get("/matches/:sportKey", cache("30 minutes"), async (req, res) => {
    try {
        const sportKey = req.params.sportKey;

        const params = new URLSearchParams({
            [apiKeyName]: apiKeyValue,
            ...url.parse(req.url, true).query,
        });

        const apiRes = await axios.get(`${apiUrl}/${sportKey}/odds/?${params}`);
        const apiResData = apiRes.data;
        const matches = apiResData.map((data) => {
            return {
                match_id: data.id,
                sport_key: sportKey,
                home_team: data.home_team,
                away_team: data.away_team,
            };
        });

        return res.status(200).json(matches);
    } catch (error) {
        res.status(500).json({ error });
    }
});

router.get(
    "/match/:sportKey/:matchId",
    cache("30 minutes"),
    async (req, res) => {
        try {
            const sportKey = req.params.sportKey;
            const matchId = req.params.matchId;
            const bookmakerKey = "betfair";

            const params = new URLSearchParams({
                [apiKeyName]: apiKeyValue,
                ...url.parse(req.url, true).query,
            });

            const apiRes = await axios.get(
                `${apiUrl}/${sportKey}/odds/?${params}`
            );
            const apiResData = apiRes.data;
            const match = apiResData
                .filter((data) => data.id === matchId)
                .map((data) => {
                    return {
                        match_id: data.id,
                        sport_key: sportKey,
                        start_time: data.commence_time,
                        home_team: data.home_team,
                        away_team: data.away_team,
                        odds: data.bookmakers.filter(
                            (bookmaker) => bookmaker.key === bookmakerKey
                        ),
                    };
                });

            return res.status(200).json(match);
        } catch (error) {
            res.status(500).json({ error });
        }
    }
);

module.exports = router;
